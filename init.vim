"" auto install vim-plug if we're starting clean
if has('vim_starting')
  set nocompatible
endif

if empty(glob('~/.config/nvim/autoload/plug.vim'))
  echo "Vim-Plug not found, installing from GitHub..."
  echo ""
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif

call plug#begin(expand('~/.config/nvim/plugged'))
" core functionality addons
Plug 'SirVer/ultisnips'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'w0rp/ale'
Plug 'itchyny/lightline.vim'
" lightline addons
Plug 'taohexxx/lightline-buffer'
Plug 'maximbaz/lightline-ale'

" general usability addons
Plug 'Yggdroot/indentLine'
Plug 'tpope/vim-surround'
Plug 'jiangmiao/auto-pairs'
Plug 'mattn/emmet-vim'
Plug 'tpope/vim-fugitive'
Plug 'ervandew/supertab'
Plug 'tpope/vim-workspace'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-vinegar'
Plug 'kien/rainbow_parentheses.vim'
Plug 'thaerkh/vim-workspace'
Plug 'roxma/nvim-yarp'
Plug 'junegunn/fzf'
Plug 'ncm2/ncm2'
Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-tmux'
Plug 'ncm2/ncm2-path'

"" Language Specific (javascript/typescript)
"Plug 'pangloss/vim-javascript'
"Plug 'neoclide/vim-jsx-improve'

" LanguageServer client for NeoVim
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
\}
let g:LanguageClient_serverCommands = {
    \ 'javascript': ['/usr/bin/javascript-typescript-stdio'],
    \ 'javascript.jsx': ['tcp://127.0.0.1:2089'],
\}
set omnifunc=LanguageClient#complete
set completefunc=LanguageClient#complete


"" Theming
Plug 'rakr/vim-one'
"Plug 'kaicataldo/material.vim'
"Plug 'joshdick/onedark.vim'
"Plug 'morhetz/gruvbox'
"Plug 'shinchu/lightline-gruvbox.vim'
call plug#end()

"
"" Color and Theme
"
set termguicolors
"let g:gruvbox_italic = 1
set background=dark
colorscheme one
"colorscheme onedark
"colorscheme gruvbox
"colorscheme material
"let g:material_theme_style = 'dark'
"let g:material_terminal_italics = 1
syntax on

"
" Plugin related configs
"
" rainbow parentheses
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

"
" emmet customizations
"
function! s:move_to_next_emmet_area(direction)
  " go to next item in a popup menu
  if pumvisible()
    if (a:direction == 0)
      return "\<C-p>"
    else
      return "\<C-n>"
    endif
  endif

  " try to determine if we're within quotes or angle brackets.
  " if so, assume we're in an emmet fill area.
  let line = getline('.')
  if col('.') < len(line)
    let line = matchstr(line, '[">][^<"]*\%'.col('.').'c[^>"]*[<"]')

    if len(line) >= 2
      if (a:direction == 0)
        return "\<Plug>(emmet-move-prev)"
      else
        return "\<Plug>(emmet-move-next)"
      endif
    endif
  endif

  " return a regular tab character
  return "\<tab>"
endfunction

" expand an emmet sequence like ul>li*5
function! s:expand_emmet_sequence()
  " expand anything emmet thinks is expandable
  if emmet#isExpandable()
    return "\<Plug>(emmet-expand-abbr)"
  endif
endfun

" IndentLine
let g:indentLine_enabled = 1
let g:indentLine_concealcursor = 0
let g:indentLine_char = '┆'
let g:indentLine_faster = 1

" lightline config (includes ale and bufferline)
set hidden
set noshowmode
set showtabline=2
let g:lightline = {
  \ 'colorscheme': 'one',
  \ 'tabline': {
  \     'left': [
  \         ['bufferinfo'],
  \         ['separator'],
  \         ['bufferbefore', 'buffercurrent', 'bufferafter'],
  \      ],
  \     'right': [['close']],
  \},
  \ 'separator': { 'left': "\ue0b0", 'right': "\ue0b2"  },
  \ 'subseparator': { 'left': "\ue0b1", 'right': "\ue0b3"  },
  \ 'component_expand': {
  \     'buffercurrent': 'lightline#buffer#buffercurrent',
  \     'bufferbefore': 'lightline#buffer#bufferbefore',
  \     'bufferafter': 'lightline#buffer#bufferafter',
  \     'linter_checking': 'lightline#ale#checking',
  \     'linter_warnings': 'lightline#ale#warnings',
  \     'linter_errors': 'lightline#ale#errors',
  \     'linter_ok': 'lightline#ale#ok',
  \},
  \ 'component_type': {
  \     'buffercurrent': 'tabsel',
  \     'bufferbefore': 'raw',
  \     'bufferafter': 'raw'
  \},
  \ 'component_function': {
  \     'gitbranch': 'fugitive#head',
  \     'gitgutter': 'MyGitGutter',
  \     'bufferinfo': 'lightline#buffer#bufferinfo',
  \},
  \ 'component': {
  \     'separator': '',
  \},
  \ 'active': {
  \     'left': [[ 'mode', 'paste' ], [ 'gitbranch', 'gitgutter', 'readonly', 'filename', 'modified' ]],
  \},
\}
let g:lightline#ale#indicator_checking = "\uf110"
let g:lightline#ale#indicator_warnings = "\uf071"
let g:lightline#ale#indicator_errors = "\uf05e"
let g:lightline#ale#indicator_ok = "\uf00c"

autocmd User ALELint call lightline#update()

let g:gitgutter_sign_added = "\u271a"
let g:gitgutter_sign_modified = "\u279c"
let g:gitgutter_sign_removed = "\u2718"
function! MyGitGutter()
  if ! exists('*GitGutterGetHunkSummary')
        \ || ! get(g:, 'gitgutter_enabled', 0)
        \ || winwidth('.') <= 90
    return ''
  endif
  let symbols = [
        \ g:gitgutter_sign_added . ' ',
        \ g:gitgutter_sign_modified . ' ',
        \ g:gitgutter_sign_removed . ' '
        \ ]
  let hunks = GitGutterGetHunkSummary()
  let ret = []
  for i in [0, 1, 2]
    if hunks[i] > 0
      call add(ret, symbols[i] . hunks[i])
    endif
  endfor
  return join(ret, ' ')
endfunction

"" ALE related settings
augroup FiletypeGroup
autocmd!
	au BufNewFile,BufRead *.jsx set filetype=javascript.jsx
augroup END

" ctrlp
set wildmode=list:longest,list:full
set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__
let g:ctrlp_custom_ignore = '\v[\/](node_modules|target|dist)|(\.(swp|tox|ico|git|hg|svn))$'
let g:ctrlp_user_command = "find %s -type f | grep -Ev '"+ g:ctrlp_custom_ignore +"'"
let g:ctrlp_use_caching = 1

" snippets
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-b>"
let g:UltiSnipsEditSplit="vertical"


"
" User configs
"

" helper functions
function WriteCreatingDirs()
    execute ':silent !mkdir -p %:h'
    write
endfunction
command W call WriteCreatingDirs()

" enable ncm2 for all buffers
autocmd BufEnter * call ncm2#enable_for_buffer()
set completeopt=noinsert,menuone,noselect
" tab to select
" and don't hijack my enter key
inoremap <expr><Tab> (pumvisible()?(empty(v:completed_item)?"\<C-n>":"\<C-y>"):"\<Tab>")
inoremap <expr><CR> (pumvisible()?(empty(v:completed_item)?"\<CR>\<CR>":"\<C-y>"):"\<CR>")

"" Required
filetype plugin indent on
"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set bomb
set binary
"" Fix backspace indent
set backspace=indent,eol,start
"" Remap leader for convenience
let mapleader=';'
"" set default identation
set expandtab
set autoindent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set laststatus=2
"" common globals
set incsearch
set nohlsearch
set ignorecase
set smartcase
set noai
"" line numbering by default
set number
set relativenumber
set wrap
set ruler
set showcmd
set shell=/usr/bin/zsh
"" disable swap files
set nobackup
set noswapfile
"" status line override
set laststatus=2
set fileformats=unix,dos,mac
"" cursor and mouse overrides
set mouse=a
set guicursor=
autocmd OptionSet guicursor noautocmd set guicursor=


"
" contextual user settings
"
" javascript
let g:javascript_enable_domhtmlcss = 1
let g:jsx_ext_required = 0 " Works on files other than .jsx
" vim-jsx-pretty
let g:vim_jsx_pretty_colorful_config = 1


" Always save line position of previous file
autocmd BufReadPost * if @% !~# '\.git[\/\\]COMMIT_EDITMSG$' && line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif 

"
" Bindings and Remaps
"
" use C-e to expand emmet syntax
autocmd FileType html,javascript.jsx,css,scss imap <buffer><expr><C-e> <sid>expand_emmet_sequence()
" use Tab or S-Tab to navigate emmet fields
autocmd FileType html,javascript.jsx,css,scss imap <buffer><expr><S-TAB> <sid>move_to_next_emmet_area(0)
autocmd FileType html,javascript.jsx,css,scss imap <buffer><expr><TAB> <sid>move_to_next_emmet_area(1)

" smarter tab bindings for buffers
nnoremap <Tab> :bn<CR>
nnoremap <S-Tab> :bp<CR>
" avoid using ESC by remapping
inoremap ;; <Esc>

" toggle line numbers
nnoremap <silent> <leader>n :set number! number?<CR>
" toggle relative line numbers
nnoremap <silent> <leader>rn :set relativenumber!<CR>
" toggle line wrap
nnoremap <silent> <leader>w :set wrap! wrap?<CR>
" toggle buffer (switch between current and last buffer)
nnoremap <silent> <leader>bb <C-^>
" go to next buffer
nnoremap <silent> <leader>bn :bn<CR>
nnoremap <C-l> :bn<CR>
" go to previous buffer
nnoremap <silent> <leader>bp :bp<CR>
" https://github.com/neovim/neovim/issues/2048
nnoremap <C-h> :bp<CR>
" close buffer
nnoremap <silent> <leader>bd :bd<CR>
" kill buffer
nnoremap <silent> <leader>bk :bd!<CR>
" list buffers
nnoremap <silent> <leader>bl :ls<CR>
" list and select buffer
nnoremap <silent> <leader>bg :ls<CR>:buffer<Space>
" horizontal split with new buffer
nnoremap <silent> <leader>bh :new<CR>
" vertical split with new buffer
nnoremap <silent> <leader>bv :vnew<CR>
" redraw screan and clear search highlighted items
"http://stackoverflow.com/questions/657447/vim-clear-last-search-highlighting#answer-25569434
nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
"" Set some shortcuts for ease-of-use
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall

" fugitive git shortcuts
"
" commit current working file
noremap <leader>gc :Gcommit %<CR>
" bring up status
noremap <leader>gs :Gstatus<CR>
" git log the current working file
noremap <leader>gl :Glog %<CR>
" git blame the current working file
noremap <leader>gb :Gblame %<CR>

"" Set working directory
nnoremap <leader>. :lcd %:p:h<CR>
"" Opens an edit command with the path of the currently edited file filled in
noremap <leader>e :e <C-R>=expand("%:p:h") . "/" <CR>
"" Opens a tab edit command with the path of the currently edited file filled
noremap <leader>te :tabe <C-R>=expand("%:p:h") . "/" <CR>

" Copy to clipboard
set clipboard=unnamedplus
vnoremap <leader>y  "+y
nnoremap <leader>Y  "+yg_
nnoremap <leader>y  "+y
nnoremap <leader>yy "+yy
" Paste from clipboard (at register 0)
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P
xnoremap p "_dP

"" Vmap for maintaining Visual Mode after shifting > and <
vmap < <gv
vmap > >gv
"" Move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Move lines up and down
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

" Like gJ, but always remove spaces
function! JoinSpaceless()
    execute 'normal gJ'

    " Character under cursor is whitespace?
    if matchstr(getline('.'), '\%' . col('.') . 'c.') =~ '\s'
        " When remove it!
        execute 'normal dw'
    endif
endfun

" Map it to a key
nnoremap <leader>J :call JoinSpaceless()<CR>

" Home goes to the beginning/end of line content
function ExtendedHome()
    let column = col('.')
    normal! ^
    if column == col('.')
        normal! 0
    endif
endfunction
noremap <silent> <Home> :call ExtendedHome()<CR>
inoremap <silent> <Home> <C-O>:call ExtendedHome()<CR>

" Remap two keys for ALE navigation
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

"" ctrlP key for ctrlP plugin
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlPMixed'

" workspace settings
let g:workspace_session_name = '.session.vim'
let g:workspace_session_disable_on_args = 1
" workspace toggle shortcuts
"nnoremap <leader>so :Obsess<CR>
"nnoremap <leader>sc :Obsess!<CR>
nnoremap <leader>ss :ToggleWorkspace<CR>

" Shortcut for commenting current line
nmap <silent> <C-/> gcc

" Gitgutter hunk mappings
nmap <leader>ha <Plug>GitGutterStageHunk
nmap <leader>hr <Plug>GitGutterUndoHunk
omap ih <Plug>GitGutterTextObjectInnerPending
omap ah <Plug>GitGutterTextObjectOuterPending
xmap ih <Plug>GitGutterTextObjectInnerVisual
xmap ah <Plug>GitGutterTextObjectOuterVisual
nmap <Leader>hn <Plug>GitGutterNextHunk
nmap <Leader>hp <Plug>GitGutterPrevHunk

" Shortcut for fixing indentation
map <F7> mzgg=G`z

" LanguageClient-neovim bindings
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

