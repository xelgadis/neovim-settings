## Requirements:

- Install PIP2/3 and the NeoVIM-PIP for Python (under ArchLinux):

`sudo pacman -Syu python-pip python2-pip python-neovim python2-neovim`

- Install PIP2/3 and the NeoVIM-PIP for Python (under Ubuntu/Debian):

`sudo apt update; sudo apt install python-pip python3-pip python-neovim python3-neovim`

## Installation instructions:

- First, clone this repo to your nvim config home, like so:

`git clone git@gitlab.com:xelgadis/neovim-settings.git ~/.config/nvim`

- For Python language completion:

`pip install python-language-server`

- For Javascript/Typescript language completion:

`yarn global add javascript-typescript-langserver`

- For fzf symbol-searching support, make sure to install the 'fzf' package for your distro!

- Vim-Plug should install automatically upon opening NeoVIM

- After the plugins update, via the NeoVIM prompt, do the following:
  - :UpdateRemotePlugins
  - :checkhealth

- ... and you're done, enjoy the development environment!
